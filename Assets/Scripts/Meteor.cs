﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour
{
    Vector2 speed;
    Transform graphics;
    public ParticleSystem ps;
    public AudioSource audioSource;
    public GameObject meteorToInstanciate;


    // Start is called before the first frame update
    void Awake()
    {
        graphics = transform.GetChild(0);

        for(int i=0; i<graphics.childCount; i++){
            graphics.GetChild(i).gameObject.SetActive(false);
        }

        int seleccionado = Random.Range(0,graphics.childCount);
        graphics.GetChild(seleccionado).gameObject.SetActive(true);

        speed.x = Random.Range(-5,-2);
        speed.y = Random.Range(-3,3);

       
    }

    void Update() {
        transform.Translate(speed*Time.deltaTime);
        graphics.Rotate(0,0,100 * Time.deltaTime);
    }
    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Finish"){
            Destroy(this.gameObject);
        } else if(other.tag == "Bullet") {
            StartCoroutine(DestroyMeteor());
        }
    }
    IEnumerator DestroyMeteor(){
        graphics.gameObject.SetActive(false);
        Destroy(GetComponent<Collider2D>());
        ps.Play();
        audioSource.Play();
        InstanceMeteors();
        yield return new WaitForSeconds(1.0f);
        Destroy(this.gameObject);
    }

    public virtual void InstanceMeteors(){
        Instantiate(meteorToInstanciate, this.transform.position, Quaternion.identity, null);
        Instantiate(meteorToInstanciate, this.transform.position, Quaternion.identity, null);
        Instantiate(meteorToInstanciate, this.transform.position, Quaternion.identity, null);
        Instantiate(meteorToInstanciate, this.transform.position, Quaternion.identity, null);
    }
}

