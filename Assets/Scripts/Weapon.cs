﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public abstract class WeaponTres: MonoBehaviour
{
    public abstract float GetCadencia();
 
    public abstract void Shoot();
}