﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorManager : MonoBehaviour
{

    public GameObject meteorPrefab;

    public float timeLauchMeteor; 

    private float currentTime = 0;
  
   
    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        if(currentTime>timeLauchMeteor){
            currentTime = 0;
            Instantiate(meteorPrefab,new Vector3(10,Random.Range(5,-5),0),Quaternion.identity,this.transform);
        }
    }
}
