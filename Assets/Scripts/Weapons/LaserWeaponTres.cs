﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class LaserWeaponTres : WeaponTres
{
 
    public GameObject laserBullet;
    public GameObject laserBulletVertical;
    public GameObject laserBulletDown;
    public float cadencia;
 
    public override float GetCadencia()
    {
        return cadencia;
    }
 
    public override void Shoot()
    {
        Instantiate (laserBullet, this.transform.position, Quaternion.identity, null);
        Instantiate (laserBulletVertical, this.transform.position, Quaternion.identity, null);
        Instantiate (laserBulletDown, this.transform.position, Quaternion.identity, null);
    }
}